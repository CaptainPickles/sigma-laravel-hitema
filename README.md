<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## about Sigma 
## MLD
<a href="https://zupimages.net/viewer.php?id=21/46/uaoq.png"><img src="https://zupimages.net/up/21/46/uaoq.png" alt="" /></a>

Pour se connecter en tant qu'admin : 

identifiant admin@admin.com
mdp : admin


pour lancé le projet :

1. créer une bdd vide
2. créer un fichier .env avec le nom de la bdd creé
3. lance wamp ou xampp et phpmy admin et mysql
4. lancé le composer install
3. Lancé la commande : 
php artisan migrate:fresh
php artisan db:seed
(pour creer les tables et lancé les seeders)
J'ai créer des factories pour les users les formations et les catégories par default toutes les formations appartiennent a l admin )
4. php artisan serve (pour lancé le serveur de dév)

Mon projet vient avec les fonctions suivantes :

listes des formations.
- des catégories par formations many to many.
- des chapitres par formations belongs to.
- des factories et des seeders.
- un système de connexion et d'admin.
- une formation appartient a 1 user .
- le user de type isAdmin = true peut modifier toutes les formations et les catégories .
- un user qui ne possède pas la formation peut juste la visionné.
- un user qui possède la formation peut modifier la formation et les chapitres associé.
- un utilisateur non connecté peut uniquement visionner les formations et leurs détails.
- un système d'inscription par formulaire.
- un système de récupération de mdp.
- une documentation via le readme

Mon projet n'a pas :
- Système de mailing et d'inscription via un email car je n'ai pas réussi et manque de temps (donc pour tester les fonctionnalités d'un utilisateur connecté non admin créer via inscription votre utilisateur)
- hebergement web vous pouvez trouver des fichier pour heberger le site sur heroku mais je n'ai pas eu le temps de finir la mise en production

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[OP.GG](https://op.gg)**
- **[CMS Max](https://www.cmsmax.com/)**
- **[WebReinvent](https://webreinvent.com/?utm_source=laravel&utm_medium=github&utm_campaign=patreon-sponsors)**
- **[Lendio](https://lendio.com)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

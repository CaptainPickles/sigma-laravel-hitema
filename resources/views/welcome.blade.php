@extends("layouts.header")
@section('content')
<div class="container">
<h1>listes des formations  </h1>
<div class="row">
@foreach($formations as $formation)
<div class="col-md-4">
    <div class="card m-3" style="width: 18rem;">
        <a  href="{{route('formationsDetails', $formation->id)}}" ><img class="card-img-top" src="{{$formation->picture}}" alt="Card image cap"></a>
        <div class="card-body">
        <h5 class="card-title">{{$formation->title}}</h5>
        <p class="card-text">{{$formation->description}}</p>
        <p class="card-text">{{$formation->prix}} $</p>
        <p>Il y a {{sizeof($formation->chapters)}} chapitre(s)</p>
        @if($formation->user)
        <p>Ecrit par {{$formation->user->name}}</p>
        @endif
        <div>
            catégories :
        @foreach($formation->categories as $category)
            <span>{{ $category->title}}</span>
        @endforeach
        </div>
        <a href="{{route('formationsDetails', $formation->id)}}" class="btn btn-primary">Apprendre</a>
        </div>
    </div>
  </div>
@endforeach
</div>
@endsection
</div>
{{-- <style>
@media (min-width: 576px) {
    .card-columns {
        column-count: 2;
    }
}

@media (min-width: 768px) {
    .card-columns {
        column-count: 3;
    }
}


@media (min-width: 1200px) {
    .card-columns {
        column-count: 3;
    }
}
  </style> --}}
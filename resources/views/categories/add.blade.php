@extends("layouts.header")
@section('content')
<div class="container">
    <h1>Ajouter une catégorie</h1>
<form method="post" action="{{route("categoryStore")}}">
    @csrf
<div class="form-group">
    <label>Nom de la catégorie</label>
    <input type="text" class="form-control" required name="title">

    <button type="submit" class="btn btn-primary">Ajouter</button>
</div>
</form>
</div>
@endsection
@extends("layouts.header")
@section('content')
<div class="container">
    @if(\Illuminate\Support\Facades\Auth::check() === false || \Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->id !== $formation->user_id )
    <h1>{{$formation->title}}</h1>
    <p>{{$formation->description}}</p>

    <p>Ecrit le {{ $formation->created_at->format('d/m/Y') }}</p>

    <a href="{{route('index')}}" >Retourner a la listes des formations</a>

    @endif
    @if(sizeof($formation->chapters) > 0)
    <h2>Listes des chapitres :</h2>
    @foreach($formation->chapters as $chapter)

    <h3>{{ $chapter->title}}</h3>
    <p>{{ $chapter->content}}</p>
    @if(\Illuminate\Support\Facades\Auth::check())
    <form action="{{route('chapitresDelete',$chapter->id)}}" method="post">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger btn-sm">Supprimer le chapitre</button>
    </form>
    @endif
    @endforeach

    @else 
    <p>Il n'y a aucun chapitre</p>

    @endif

    @if($errors->any())
<ul class="alert alert-danger">
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
@endif
@if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->id == $formation->user_id
|| \Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->isAdmin === 1)
<h2>Modifier une formation</h2>
<form method="POST" action="{{route('formationUpdate',$formation->id)}}" enctype="multipart/form-data">
    @csrf
    @method('POST')
    <div class="form-group">
        <label>Titre</label>
        <input type="text" class="form-control" placeholder="" name="title" value="{{$formation->title}}" >
    </div>

    <div class="form-group">
        <label>Description</label>
        <textarea type="text" class="form-control" rows="5" name="description"  >{{$formation->description}}</textarea>
    </div>

    <div class="form-group">
        <label>prix</label>
        <input type="number" class="form-control" placeholder="" name="prix" value="{{$formation->prix}}">
    </div>

    <div class="form-group">
        <label>picture</label>
        <input type="text" class="form-control" placeholder="" name="picture" value="{{$formation->picture}}">
    </div>

    <div>
        @foreach($categories as $category)
            <div class="form-check form-check-inline">
                <input type="checkbox" class="form-check-input" id="check-{{$category->id}}"
                       name="checkboxCategories[{{$category->id}}]"
                       value="{{$category->id}}"
                       @if($formation->categories->contains('id', $category->id)) checked @endif>
                <label for="check-{{$category->id}}" class="form-check-label">{{$category->title}}</label>
            </div>
        @endforeach
    </div>

    <button type="submit" class="btn btn-primary">Modifier la formation</button>
</form>

<form method="post" action="{{route('formationDelete',$formation->id)}}">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-danger">Supprimer la formation</button>
</form>

<h1>Ajouter un chapitre </h1>
<form method="POST" action="{{route('chapitresStore',$formation->id)}}" enctype="multipart/form-data">
    @csrf
    @method('POST')
    <div class="form-group">
        <label>Titre</label>
        <input type="text" class="form-control" placeholder="" name="title" >
    </div>

    <div class="form-group">
        <label>contenue</label>
        <textarea type="text" class="form-control" rows="5" name="content" ></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Ajouter un chapitre</button>
</form>
@endif
</div>
@endsection

</div>

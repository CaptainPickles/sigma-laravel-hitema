<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ChapterController;
use App\Http\Controllers\FormationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FormationController::class,'index'])->name("index");
Route::get('/formation/ajouter', [FormationController::class,'add'])->name("formationAdd")->middleware("auth");
Route::post('/formation/ajouter', [FormationController::class,'store'])->middleware("auth");;
Route::get('/formation/{id}', [FormationController::class,'details'])->name("formationsDetails");
Route::post('/formation/{id}', [FormationController::class,'update'])->name("formationUpdate")->middleware("auth");;
Route::delete('/formation/{id}', [FormationController::class,'delete'])->name("formationDelete")->middleware("auth");;
Route::post('/chapitres/{formationId}',[ChapterController::class, "store"])->name("chapitresStore")->middleware("auth");;
Route::delete('/chapitres/{id}',[ChapterController::class, "delete"])->name("chapitresDelete")->middleware("auth");;
Route::get('/categories/ajouter',[CategoryController::class, "add"])->name("categoryAdd")->middleware("auth");;
Route::post('/categories/ajouter',[CategoryController::class, "store"])->name("categoryStore")->middleware("auth");;
Route::get('/categories',[CategoryController::class, "index"])->name("categoryList");



Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

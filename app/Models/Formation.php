<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    use HasFactory;

    protected $table ="formations";

    protected $fillable = ["title","description",'prix','picture',"user_id"];

    public function chapters(){
        return $this->hasMany(Chapter::class,'formations', "id");
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'formations_categories',"formation","category");
    }
    public function user(){
        return $this->hasOne(User::class,'id',"user_id");
    }
}

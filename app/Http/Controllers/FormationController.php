<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormationStoreRequest;
use App\Models\Category;
use App\Models\Formation;
use Illuminate\Http\Request;

class FormationController extends Controller
{
    
    public function index(){
        $formations = Formation::all();
        return view('welcome')->with('formations', $formations);
    }

public function details($id){
    $formation = Formation::find($id);
    $categories = Category::all();
    return view("formations.details", compact(['formation',"categories"]));
}

public function add(){
    $categories = Category::all();
    return view("formations.add",compact(['categories']));
}

public function store(FormationStoreRequest $request){
    $params = $request->validated();
    $params["user_id"] = auth()->user()->id;
    $created = Formation::create($params);

    $created->categories()->detach();
    if(!empty($params['checkboxCategories'])){
        $created->categories()->attach($params['checkboxCategories']);
    }

    return redirect()->route("index");

}

public function update($id,FormationStoreRequest $request){
    $params = $request->validated();
    $formation = Formation::find($id);
    $formation->update($params);

    $formation->categories()->detach();
    if(!empty($params['checkboxCategories'])){
        $formation->categories()->attach($params['checkboxCategories']);
    }
    return redirect()->route("formationsDetails",$id);
}

public function delete($id){
    $formation = Formation::find($id);
    $formation->delete();

    return redirect()->route("index");
}
}
